import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  imageHome: string = '../../../assets/images/logo_home_01.png';

  constructor(private readonly route: Router) {}

  ngOnInit(): void {}

  redirect(): void {
    this.route.navigateByUrl(`/pets`);
  }
}
