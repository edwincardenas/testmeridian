import { Component, OnInit } from '@angular/core';
import { PetsService } from './pets.service';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { IPetsHttpModel } from './models/http/pets.http.model';

@UntilDestroy()
@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.scss'],
})
export class PetsComponent implements OnInit {
  typePetInput: string = '';
  imagePet: string[] = [];
  disabled: boolean = true;
  cardVisible: boolean = false;
  notificationMessage: string[] = [];
  subBreed: string[] = [];
  subBreedImages: string[] = [];
  isLoading: boolean = false;

  constructor(private readonly servicePet: PetsService) {}

  detectInput(e: string): void {
    if (e.length > 0) {
      this.disabled = false;
    } else {
      this.disabled = true;
      this.cardVisible = false;
      this.notificationMessage = [];
      this.subBreed = [];
    }
  }

  ngOnInit(): void {}

  searchPet(): void {
    this.cardVisible = false;
    this.isLoading = true;
    this.subBreed = [];

    this.servicePet
      .getPetByBreed(this.typePetInput.toLocaleLowerCase())
      .pipe(untilDestroyed(this))
      .subscribe((response: IPetsHttpModel.IReponseData): void => {
        if (response.status == 'success') {
          this.searchSub();
          this.imagePet = response.message;
          this.notificationMessage = [];
        } else if (response.status == 'error') {
          this.cardVisible = false;
          this.notificationMessage = response.message;
        }
        this.isLoading = false;
      });
  }

  searchSub(): void {
    this.servicePet
      .getPetSubBreed(this.typePetInput.toLocaleLowerCase())
      .pipe(untilDestroyed(this))
      .subscribe((response: IPetsHttpModel.IReponseData): void => {
        if (response.status == 'success') {
          if (response.message.length > 0) {
            this.subBreed = response.message;
          } else {
            this.subBreed[0] = 'Does not have sub-races';
          }
          this.cardVisible = true;
        } else if (response.status == 'error') {
          this.cardVisible = false;
          this.notificationMessage = response.message;
        }
      });
  }

  getSubBreed(subBreed: string): void {
    this.servicePet
      .getPetAllSubBreed(this.typePetInput.toLocaleLowerCase(), subBreed)
      .pipe(untilDestroyed(this))
      .subscribe((response: IPetsHttpModel.IReponseData): void => {
        if (response.status == 'success') {
          this.imagePet = response.message;
          this.notificationMessage = [];
        } else if (response.status == 'error') {
          this.cardVisible = false;
          this.notificationMessage = response.message;
        }
      });
  }
}
