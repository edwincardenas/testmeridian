import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';
import { IPetsHttpModel } from './models/http/pets.http.model';

@Injectable({
  providedIn: 'root'
})
export class PetsService {

  constructor(private http: HttpClient) { }

  private readonly router: string = "https://dog.ceo/api/breed/";
  private readonly routerAll: string = "https://dog.ceo/api/breeds/";

  getPetByBreed(typePet : string) {
    let response: IPetsHttpModel.IReponseData;
    return this.http.get(`${this.router}` + typePet + `/images`).pipe(
      map((request: any): IPetsHttpModel.IReponseData => {

        const status = request?.['status'];

        if(status == 'success'){
          response =  request;
        }

        return response;
      }),
      // @ts-ignore: Unreachable code error
      catchError((responseError: HttpErrorResponse): Observable<IPetsHttpModel.IReponseData> => {
        if (responseError) {
          return of(responseError.error);
        }
      })
    );
  }

  getPetSubBreed(typePet : string) {
    let response: IPetsHttpModel.IReponseData;
    return this.http.get(`${this.router}` + typePet + `/list`).pipe(
      map((request: any): IPetsHttpModel.IReponseData => {

        const status = request?.['status'];

        if(status == 'success'){
          response =  request;
        }

        return response;
      }),
      // @ts-ignore: Unreachable code error
      catchError((responseError: HttpErrorResponse): Observable<IPetsHttpModel.IReponseData> => {
        if (responseError) {
          return of(responseError.error);
        }
      })
    );
  }

  getPetAllSubBreed(breed : string, subBreed : string) {
    let response: IPetsHttpModel.IReponseData;
    return this.http.get(`${this.router}`+ `${breed}/`+  `${subBreed}/images`).pipe(
      map((request: any): IPetsHttpModel.IReponseData => {

        const status = request?.['status'];

        if(status == 'success'){
          response =  request;
        }

        return response;
      }),
      // @ts-ignore: Unreachable code error
      catchError((responseError: HttpErrorResponse): Observable<IPetsHttpModel.IReponseData> => {
        if (responseError) {
          return of(responseError.error);
        }
      })
    );
  }


}
