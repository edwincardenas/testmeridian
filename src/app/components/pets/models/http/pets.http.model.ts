export namespace IPetsHttpModel { 


export interface IReponseData { 
  code: number | null;
  message : string[];
  status : string;

}

export const DEFAULT_DATA_RESPONSE : IReponseData = {
  code : 0,
  message : [],
  status : '',
}

}
